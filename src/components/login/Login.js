import LoginForm from './LoginForm'
import { useHistory } from 'react-router-dom';
import './Login.module.css';

const Login = () => {
    const history = useHistory();

    function onSubmitHandler(e) {
        e.preventDefault();
        console.log('submited', e);
        history.replace('/principal')
    }


    return (
        <div className='container gradient'>
            <div className='row'>
                <div className='col-7'>

                </div>
                <div className='col-5'>
                    <LoginForm onSubmitHandler={onSubmitHandler} />
                </div>
            </div>
        </div>
    )
}

export default Login
