const LoginForm = (props) => {
	return (
		<form onSubmit={props.onSubmitHandler}>
			<div className='container'>
				<div className='form-group row'>
					<label htmlFor="username" className='form-label col-sm-12 col-md-3'>Usuario</label>
					<input className='form-control col-sm-12 col-md-9' id='username' type="text" placeholder='Ingrese el usuario' />
				</div>
			</div>
		
			<div className='container'>
				<div className='form-group row'>
					<label htmlFor="password" className='form-label col-sm-12 col-md-3'>Contraseña</label>
					<input className='form-control col-sm-12 col-md-9' type="password" placeholder='Ingrese la contraseña' />
				</div>
			</div>
			
			<button type='submit' className='btn btn-primary'>Iniciar Sesión</button>
		
		</form>
		)
	}
	
	export default LoginForm
	