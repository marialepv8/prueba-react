import { Link } from 'react-router-dom'

const NavBar = () => {
    return (
        <header>
            <ul className="nav justify-content-end">
                <li className="nav-item">
                    <Link to='/Principal' className="nav-link">Principal</Link>
                </li>
                <li className="nav-item">
                    <Link to='/' className="nav-link">Cerrar Sesión</Link>
                </li>
            </ul>
        </header>
        
    )
}

export default NavBar
