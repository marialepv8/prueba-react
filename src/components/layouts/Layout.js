import NavBar from './NavBar'
import { useLocation } from 'react-router-dom'

const Layout = (props) => {
    const location = useLocation();
    const x = location.pathname === '/'

    return (
        <div >
            {
                !x ? <NavBar /> : null
            }
            <div >
                {props.children}
            </div>
            {
                !x ? <footer>Footer</footer> : null
            }
        </div>
    )
}

export default Layout
