import { Route, Switch } from 'react-router-dom';
import Layout from './components/layouts/Layout';
import Login from './components/login/Login';
import Principal from './components/Principal';

function App() {
  return (
    <Layout>
      <Switch>
        <Route path='/' exact>
          <Login />
        </Route>
        <Route path='/principal'>
          <Principal />
        </Route>
      </Switch>
    </Layout>
  );
}

export default App;
